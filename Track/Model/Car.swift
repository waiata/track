//
//  Car.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

class Car: Codable {
    
    //MARK: Properties
    
    var number: String
    
    var int: Int {
        return Int(number) ?? 0
    }
    
    //MARK: Coding
    
    enum CodingKeys: String, CodingKey {
        case number
    }
    
    //MARK: Init
    
    init(number: Int) {
        self.number = String(number)
    }
}
