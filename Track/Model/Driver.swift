//
//  Driver.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

class Driver: Codable {

    //MARK: Properties
    
    var name: String = ""
    
    var short: String?
    
    var shortName: String {
        return short ?? name.prefix(3).uppercased()
    }
    
    //MARK: Coding
    
    enum CodingKeys: String, CodingKey {
        case name
        case short
    }

}
