//
//  Entry.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

class Entry: Codable {

    //MARK: Properties
    
    var car: Car
    var driver: Driver
    
    var place: Int?
    var laps: Int = 0
    var gap: Double = 0
    var fastestLap: TimeInterval? {
        didSet {
            time = fastestLap?.string(format: "M:ss.fff") ?? ""
        }
    }
    
    var time: String = ""
    
    /// place for sorting
    var position: Int {
        return place ?? Int.max
    }
    
    /// fastest lap for sorting
    var fastest: TimeInterval {
        return fastestLap ?? Double(Int.max)
    }
    
    //MARK: Coding
    
    enum CodingKeys: String, CodingKey {
        case car
        case driver
        case place
        case laps
        case gap
        case fastestLap
        case time
    }
    
    //MARK: Init
    
    init(car: Int) {
        self.car = Car(number: car)
        driver = Driver()
    }
}

extension Entry: Equatable {
    static func == (lhs: Entry, rhs: Entry) -> Bool {
        return lhs === rhs
    }
    
    
}
