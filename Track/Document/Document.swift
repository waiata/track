//
//  Document.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class Document: NSDocument {
    
    var race = Race()
    
    //MARK: Init
    override init() {
        super.init()
        race.document = self
    }
    
    //MARK: Open
    
    override func read(from data: Data, ofType typeName: String) throws {
        do {
            let decoder = JSONDecoder()
            race = try decoder.decode(Race.self, from: data)
            race.document = self
        }
        catch {
            throw DocumentError.jsonDecoding
        }
    }
    
    //MARK: Save
    
    override func data(ofType typeName: String) throws -> Data {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        do {
            let data = try encoder.encode(race)
            return data
        } catch {
            throw DocumentError.jsonEncoding
        }
    }
    
    //MARK: Windows
    
    override func makeWindowControllers() {
        let raceConsole = Scene(name: "Race Window")
        raceConsole.open(self)
    }
    
    @IBAction func showRaceConsole(_ sender: Any) {
        Scene.raceWindow.open(self)
    }
    
    @IBAction func showTimeConsole(_ sender: Any) {
        Scene.timeWindow.open(self)
    }
    
    //MARK: Static
    
    override class var autosavesInPlace: Bool {
        return true
    }
    
    //MARK: Errors
    enum DocumentError: Error {
        case jsonDecoding
        case jsonEncoding
    }
    
    
    
    
}
