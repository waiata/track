//
//  Race.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

class Race: Codable {
    
    //MARK: Connections
    
    weak var document: Document?
    
    //MARK: Properties
    
    var entries = [Entry]()
    
    
    //MARK: Coding
    
    enum CodingKeys: String, CodingKey {
        case entries
    }
    
    //MARK: Entries
    
    func newEntry() {
        let entry = Entry(car: nextNumber)
        entries.append(entry)
    }
    
    var nextNumber: Int {
        return (entries.map{ $0.car.int }.max() ?? 0) + 1
    }
    
    //MARK: Stats
    
    var fastestLap: TimeInterval? {
        return entries.compactMap{ $0.fastestLap }.min()
    }
    
    //MARK: Render
    
    var raceConsoles: [RaceConsole] {
        return document?.windowControllers.map{ $0.contentViewController }.filter{ $0 is RaceConsole } as? [RaceConsole] ?? []
    }
    
    /// render all consoles
    func render() {
        for console in raceConsoles {
            console.render()
        }
    }
    
    //MARK: Record
    
    func record() {
        document?.save(self)
    }
}
