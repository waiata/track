//
//  RaceConsole.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class RaceConsole: Console {
    
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        raceTable.delegate = self
        raceTable.dataSource = self
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        render()
    }
    
    //MARK: Render
    
    func render() {
        entries = race?.entries ?? []
        sort()
        raceTable.reloadData()
        record()
    }
    
    //MARK: Entries
    
    var entries = [Entry]()
    
    //MARK: Record
    
    func record() {
        race?.entries = entries
        race?.record()
    }
    
    //MARK: Outlets
    
    @IBOutlet weak var raceTable: NSTableView!
    
    //MARK: Selection
    
    var selectedEntries: [Entry] {
        return raceTable.selectedRowIndexes.sorted().map{ entries[$0] }
    }
    
    /// Select rows with given entries
    /// called after reload
    /// - Parameter entries: arraqy od Entry objects to select
    func select(_ selection: [Entry]) {
        raceTable.deselectAll(self)
        for (row, entry) in entries.enumerated() {
            if selection.contains(entry) {
                raceTable.selectRowIndexes([row], byExtendingSelection: true)
            }
        }
    }
    
    //MARK: Actions
    
    @IBAction func addEntry(_ sender: Any) {
        race?.newEntry()
        render()
    }
    
    @IBAction func placeUp(_ sender: Any) {
        let selected = selectedEntries
        for entry in selected {
            let place = max((entry.place ?? entries.count) - 1, 1)
            let passed = entries.filter{ $0.place == place }
            for loser in passed {
                loser.place = entry.place
            }
            entry.place = place
        }
        render()
        select(selected)
    }
    
    @IBAction func placeDown(_ sender: Any) {
        let selected = selectedEntries
        for entry in selectedEntries.reversed() {
            let place = min((entry.place ?? entries.count) + 1, entries.count)
            let passed = entries.filter{ $0.place == place }
            for loser in passed {
                loser.place = entry.place
            }
            entry.place = place
        }
        render()
        select(selected)
    }
    
    @IBAction func removeEntries(_ sender: Any) {
        for row in raceTable.selectedRowIndexes.sorted().reversed() {
            entries.remove(at: row)
        }
        race?.entries = entries
        render()
    }
    
    @IBAction func resetFastestLaps(_ sender: Any) {
        for entry in selectedEntries {
            entry.fastestLap = nil
        }
        render()
    }
    
    @IBAction func rePlace(_ sender: NSTextField) {
        guard let entry = entry(for: sender) else { return }
        entry.place = sender.integerValue
    }
    
    @IBAction func renumberCar(_ sender: NSTextField) {
        guard let entry = entry(for: sender) else { return }
        entry.car.number = sender.stringValue
    }
    
    @IBAction func renameDriver(_ sender: NSTextField) {
        guard let entry = entry(for: sender) else { return }
        entry.driver.name = sender.stringValue
    }
    
    @IBAction func relap(_ sender: NSTextField) {
        guard let entry = entry(for: sender) else { return }
        entry.laps = sender.integerValue
    }
    
    @IBAction func regap(_ sender: TimeField) {
        guard let entry = entry(for: sender) else { return }
        entry.gap = sender.seconds
    }
    
    @IBAction func refast(_ sender: TimeField) {
        guard let entry = entry(for: sender) else { return }
        if sender.stringValue.isEmpty { entry.fastestLap = nil }
        else { entry.fastestLap = sender.seconds }
    }
    
    func entry(for view: NSView) -> Entry? {
        let row = raceTable.row(for: view)
        guard entries.indices.contains(row) else { return nil }
        return entries[row]
    }
    
}

extension RaceConsole: NSTableViewDelegate, NSTableViewDataSource {
    
    //MARK: Table
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return entries.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let entry = entries[row]
        
        switch tableColumn?.identifier {
        case Table.rankColumn: return rankCell(for: row)
        case Table.placeColumn: return placeCell(for: entry)
        case Table.carColumn: return carCell(for: entry)
        case Table.driverColumn: return driverCell(for: entry)
        case Table.lapsColumn: return lapsCell(for: entry)
        case Table.gapColumn: return gapCell(for: entry)
        case Table.fastestColumn: return fastestCell(for: entry)
        default: return nil
        }
        
    }
    
    func rankCell(for row: Int) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.rankCell, owner: nil) as? NSTableCellView
        view?.textField?.integerValue = row + 1
        return view
    }
    func placeCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.placeCell, owner: nil) as? NSTableCellView
        if let place = entry.place {
            view?.textField?.integerValue = place
        } else {
            view?.textField?.stringValue = ""
        }
        return view
    }
    func carCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.carCell, owner: nil) as? NSTableCellView
        view?.textField?.stringValue = entry.car.number
        return view
    }
    func driverCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.driverCell, owner: nil) as? NSTableCellView
        view?.textField?.stringValue = entry.driver.name
        return view
    }
    func lapsCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.lapsCell, owner: nil) as? NSTableCellView
        view?.textField?.integerValue = entry.laps
        return view
    }
    func gapCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.gapCell, owner: nil) as? NSTableCellView
        (view?.textField as? TimeField)?.seconds = entry.gap
        return view
    }
    func fastestCell(for entry: Entry) -> NSView? {
        let view = raceTable.makeView(withIdentifier: Table.fastestCell, owner: nil) as? NSTableCellView
        if let seconds = entry.fastestLap {
            (view?.textField as? TimeField)?.seconds = seconds
        } else {
            view?.textField?.stringValue = ""
        }
        return view
    }
    
    
    //MARK: Sorting
    
    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        sort()
        tableView.reloadData()
        record()
    }
    
    
    func sort() {
        guard let descriptor = raceTable.sortDescriptors.first else { return }
        switch descriptor.key {
        case "place":
            descriptor.ascending ? entries.sort { $0.position < $1.position } : entries.sort { $0.position > $1.position }
        case "car":
            descriptor.ascending ? entries.sort { $0.car.int < $1.car.int } : entries.sort { $0.car.int > $1.car.int }
        case "driver":
            descriptor.ascending ? entries.sort { $0.driver.name < $1.driver.name } : entries.sort { $0.driver.name > $1.driver.name }
        case "laps":
            descriptor.ascending ? entries.sort { $0.laps < $1.laps } : entries.sort { $0.laps > $1.laps }
        case "gap":
            descriptor.ascending ? entries.sort { $0.gap < $1.gap } : entries.sort { $0.gap > $1.gap }
        case "fastest":
            descriptor.ascending ? entries.sort { $0.fastest < $1.fastest } : entries.sort { $0.fastest > $1.fastest }
        default: return
        }
        
        
    }
    
    
    //MARK Identifiers
    
    struct Table {
        
        static let rankColumn = NSUserInterfaceItemIdentifier("Race Rank Column")
        static let rankCell = NSUserInterfaceItemIdentifier("Race Rank Cell")
        static let placeColumn = NSUserInterfaceItemIdentifier("Race Place Column")
        static let placeCell = NSUserInterfaceItemIdentifier("Race Place Cell")
        static let carColumn = NSUserInterfaceItemIdentifier("Race Car Column")
        static let carCell = NSUserInterfaceItemIdentifier("Race Car Cell")
        static let driverColumn = NSUserInterfaceItemIdentifier("Race Driver Column")
        static let driverCell = NSUserInterfaceItemIdentifier("Race Driver Cell")
        static let lapsColumn = NSUserInterfaceItemIdentifier("Race Laps Column")
        static let lapsCell = NSUserInterfaceItemIdentifier("Race Laps Cell")
        static let gapColumn = NSUserInterfaceItemIdentifier("Race Gap Column")
        static let gapCell = NSUserInterfaceItemIdentifier("Race Gap Cell")
        static let fastestColumn = NSUserInterfaceItemIdentifier("Race Fastest Column")
        static let fastestCell = NSUserInterfaceItemIdentifier("Race Fastest Cell")
        
    }
}

