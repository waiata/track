//
//  Scene.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

struct Scene {
    
    var name: String
    var board: String? = nil
    
    var storyboard: NSStoryboard? {
        if let name = board {
            return NSStoryboard(name: name, bundle: nil)
        } else {
            return NSStoryboard.main
        }
    }
    
    var identifier: NSStoryboard.SceneIdentifier {
        return NSStoryboard.SceneIdentifier(name)
    }
    
    func open(_ doc: Document? = nil) {
        let controller = storyboard?.instantiateController(withIdentifier: identifier)
        
        if let wc = controller as? NSWindowController {
            doc?.addWindowController(wc)
            wc.showWindow(self)
        }

    }
    
    static let raceWindow = Scene(name: "Race Window")
    static let timeWindow = Scene(name: "Time Window")

    
}
