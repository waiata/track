//
//  Console.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/29.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class Console: NSViewController {

    //MARK: Connections
    
    var document: Document? {
        return view.window?.windowController?.document as? Document
    }
    
    var race: Race? {
        return document?.race
    }
    
    
}
