//
//  TimeConsole.swift
//  Track
//
//  Created by Neal Watkins on 2020/10/30.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class TimeConsole: Console {

    //MARK: Entries
    
    /// Find the Entry for car number
    /// - Parameter number: number of the car
    func entry(number: String) -> Entry? {
        return race?.entries.first(where: { $0.car.number == number})
    }
     
     //MARK: Outlets
     
     @IBOutlet weak var carField: NSTextField!
     @IBOutlet weak var timeField: TimeField!
     
     
     //MARK: Actions
     
     @IBAction func enterTime(_ sender: Any) {
        guard let entry = entry(number: carField.stringValue) else { return }
        if timeField.stringValue.isEmpty {
            entry.fastestLap = nil
        } else {
            let currentBest = entry.fastest
            let newBest = timeField.seconds
            entry.fastestLap = min(currentBest, newBest)
        }
        renderRace()
//        record()
     }
    
    /// reload race table
    func renderRace() {
        race?.render()
    }
    
    /// save data
    func record() {
        race?.document?.save(self)
    }
     
    
}
