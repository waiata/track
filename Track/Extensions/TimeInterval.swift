//
//  TimeInterval.swift
//  Bounce
//
//  Created by Neal on 4-9-17.
//  Copyright © 2017 waiata. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    static var sinceMidnight : TimeInterval {
        let now = Date()
        let cal = Calendar(identifier: .gregorian)
        let midnight = cal.startOfDay(for: now)
        return now.timeIntervalSince(midnight)
    }
    
    var totalHours : Int {
        return Int(self/3600.0)
    }
    
    var hours : UInt {
        return UInt(abs(self)/3600.0) % 24
    }
    
    var totalMinutes : Int {
        return Int(self/60.0)
    }
    
    var minutes : Int {
        return Int(abs(self)/60.0) % 60
    }
    
    var totalSeconds : Int {
        return Int(self)
    }
    
    var seconds : Int {
        return Int(abs(self)) % 60
    }
    
    var tenths : Int {
        return Int(abs(self) * 10) % 10
    }
    
    var hundredths : Int {
        return Int(abs(self) * 100) % 100
    }
    
    var thousandths : Int {
        return Int(abs(self) * 1000) % 1000
    }
    
    func string(format: String = "S.F") -> String {
        var qwerty = format
        qwerty = qwerty.replacingOccurrences(of: "fff", with: String(format: "%03d", thousandths))
        qwerty = qwerty.replacingOccurrences(of: "ff", with: String(format: "%02d", hundredths))
        qwerty = qwerty.replacingOccurrences(of: "f", with: String(format: "%01d", tenths))
        qwerty = qwerty.replacingOccurrences(of: "F", with: String(format: "%01d", tenths))
        qwerty = qwerty.replacingOccurrences(of: "ss", with: String(format: "%02d", seconds))
        qwerty = qwerty.replacingOccurrences(of: "S", with: "\(totalSeconds)")
        qwerty = qwerty.replacingOccurrences(of: "mm", with: String(format: "%02d", minutes))
        qwerty = qwerty.replacingOccurrences(of: "M", with: "\(totalMinutes)")
        qwerty = qwerty.replacingOccurrences(of: "h", with: "\(hours)")
        qwerty = qwerty.replacingOccurrences(of: "hh", with: String(format: "%02d", hours))
        qwerty = qwerty.replacingOccurrences(of: "H", with: "\(totalHours)")
        
        return qwerty
    }
    
    //MARK: Strings
    
    var m : String {
        return "\(totalMinutes)"
    }
    
    var ss : String {
        return String(format: "%02d", seconds)
    }
    
    var hh : String {
        return String(format: "%02d", hundredths)
    }
    
    var msshh : String {
        return "\(m):\(ss).\(hh)"
    }
    
    var mmmss : String {
        return "\(m):\(ss)"
    }
    
}
